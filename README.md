# Legacy Snekctl

This is the current solution running on a raspberry pi zero. It controls the temperature regulation an lights for our hognose snakes. The current solution is being managed by systemd on the raspberry pi.

I am currently implementing at new microservice based solution, to take over. The repository of the new microservice can be found [here](https://gitlab.com/FarmCreek/snekctlv).
