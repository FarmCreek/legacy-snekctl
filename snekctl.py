import glob
import time
from datetime import date, datetime
from gpiozero import DigitalOutputDevice


def read_temperature_raw(path): 
    with open(path, 'r') as file:
        lines = file.readlines() 

    return lines



def read_temperature(device):
    temperature = -1      
    device_path = (glob.glob('/sys/bus/w1/devices/' + device)[0] 
                    + '/w1_slave')

    lines = read_temperature_raw(device_path)
    while len(lines[0]) > 0 and lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temperature_raw(device_path)
    
    equals_postition = lines[1].find('t=')
    if equals_postition != -1:
        temperature = float(lines[1][equals_postition+2:]) / 1000.0 

    return temperature



def light_control(light, hour):    
    if(hour < 6 or hour >= 22):
        light.off()
    else:
        light.on()



def temperature_control(sensor, heater, hour):
    temperature = read_temperature(sensor)
    if hour >= 6 and  hour < 22:
        if temperature > 31.0:
            heater.off() 
        if temperature < 30.0:
            heater.on()
            
    else:
        if temperature > 23.0:
            heater.off() 
        if temperature < 21.0:
            heater.on() 
  

 
if __name__ == "__main__":
    light = DigitalOutputDevice(22)

    temperature_sensor_nibbler = '10-0008039ae23b'
    temperature_sensor_bobbles = '10-0008039a0640'

    heater_nibbler = DigitalOutputDevice(17)
    heater_bobbles = DigitalOutputDevice(27)

    print('Started control loop')
    while True:
        current_hour = datetime.now().hour
        light_control(light, current_hour)

        print(f"Current time is: {datetime.now()}")
        print(f"Nibbler temperature is: {read_temperature(temperature_sensor_nibbler)}")
        print(f"Bobbles temperature is: {read_temperature(temperature_sensor_bobbles)}\n")

        temperature_control(temperature_sensor_nibbler, 
            heater_nibbler, current_hour)
        temperature_control(temperature_sensor_bobbles,
            heater_bobbles, current_hour)
        
        time.sleep(1)

    print('Control loop ended!')
